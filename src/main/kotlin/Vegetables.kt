class Vegetables(vararg val toppings:String) : Item("Vegetables",5) {

    override fun toString(): String {
        if (toppings.isEmpty()) {
            return "$name Chef's choice"
        } else {
            return name + " " + toppings.joinToString ()
        }
    }
}